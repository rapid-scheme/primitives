;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;;; Procedures

(define (insert-primitive! primitive)
  (insert-binding! (syntax ,primitive)
		   (make-denotation primitive #f)))

;;;; Values

(define primitives
  '(quote
    define-values define-syntax
    begin
    set!
    if
    lambda
    syntax-error
    cond-expand include include-ci
    syntax-rules ... _
    
    exit
    
    fx+))

;;> \section{Objects}

;;> \procedure{rapid-primitive-exports}

;;> A mapping in the sense of \scheme{(rapid analyze-library)} that
;;> describes the exports of the library \scheme{(rapid primitive)}.

(define rapid-primitive-exports
  (mapping-unfold null?
		  (lambda (primitives)
		    (let ((primitive (car primitives)))
		      (values primitive
			      (make-qualified-identifier #f
							 '(rapid primitive)
							 primitive))))
		  cdr
		  primitives
		  symbol-comparator))

;;> \procedure{rapid-primitive-environment}

;;> The syntactic environment of the library \scheme{(rapid
;;> primitive)}.

(define rapid-primitive-environment
  (parameterize ((current-syntactic-environment
		  (make-syntactic-environment)))
    (for-each insert-primitive! primitives)
    (current-syntactic-environment)))

;;> \procedure{rapid-primitive-store}

;;> A list in the sense of \scheme{(rapid evaluate-library)}
;;> describing the values stored in the locations exported by the
;;> library \scheme{(rapid primitive)}.

(define rapid-primitive-store
  '())
