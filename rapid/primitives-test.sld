;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid primitives-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid mapping)
	  (rapid syntactic-environment)
	  (rapid primitives))
  (begin
    (define (run-tests)
    
      (test-begin "Library of primitives")

      (test-assert "rapid-primitive-exports"
	(mapping? rapid-primitive-exports))

      (test-assert "rapid-primitive-environment"
	(syntactic-environment? rapid-primitive-environment))

      (test-assert "rapid-primitive-store"
	(list? rapid-primitive-store))
      
      (test-end))))
